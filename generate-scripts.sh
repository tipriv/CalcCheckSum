#!/bin/bash  -xue

convert_to_sjis_command='/usr/local/bin/nkf -W8 -s -Lw'

if [[ $# -lt 1 ]] ; then
    echo  "Usage: $0 (target dir)"  1>&2
    exit  2
fi

target_dir=$1
shift  1

# 末尾のスラッシュは取り除いておく
target_dir=${target_dir%/}

target_name=${target_dir##*/}
if [[ ${target_name} =~ ^.*(Disc-[0-9]+).*$ ]] ; then
    disc_id=${BASH_REMATCH[1]}
else
    disc_id=${target_name}
fi


fullpath_cygwin=$(readlink  -f  ${target_dir})
fullpath_window=$(cygpath   -w  ${fullpath_cygwin})

gen_script_sjis="Make.md5.${disc_id}.sjis.bat"
gen_script_utf8="Make.md5.${disc_id}.utf8.sh"

output_file_sjis="${disc_id}.sjis.md5"
output_file_utf8="${disc_id}.utf8.md5"


cat  <<-  __EOS__  |  tee      ${gen_script_utf8}
#!/bin/bash  -xu

pushd  ${fullpath_cygwin}

__EOS__

cat  <<-  __EOS__   \
    |  ${convert_to_sjis_command}       \
    |  tee      ${gen_script_sjis}
ECHO  ON
PUSHD  ${fullpath_window}

__EOS__


for  dir  in  $(find ${target_dir} -type d) ; do
    if [[ "X${dir}Y" = "X.Y" ]] ; then
        continue
    fi
    if [[ "X${dir}Y" = "X..Y" ]] ; then
        continue
    fi

    fullpath_cygwin=$(readlink  -f  ${dir})
    fullpath_window=$(cygpath   -w  ${fullpath_cygwin})

    if [[ "${fullpath_cygwin}" = "${target_dir}" ]] ; then
        continue
    fi

    dir_name=${fullpath_cygwin%/}
    dir_last_name=${dir_name##*/}
    if [[ "${dir_last_name}" =~ ^([A-Za-z0-9_-])+$ ]] ; then
        file_prefix=".${dir_last_name}"
    else
        file_prefix=''
    fi
    output_file_sjis="${disc_id}${file_prefix}.sjis.md5"
    output_file_utf8="${disc_id}${file_prefix}.utf8.md5"

cat  <<-  __EOS__  |  tee  -a  ${gen_script_utf8}
cd  ${fullpath_cygwin}
md5sum  -b *.wmv  |  tee  ${output_file_utf8}

__EOS__

cat  <<-  __EOS__   \
    |  ${convert_to_sjis_command}       \
    |  tee  -a  ${gen_script_sjis}
cd  ${fullpath_window}
md5sum.exe  -b *.wmv  >   ${output_file_sjis}
type  ${output_file_sjis}

__EOS__

done

cat  <<-  __EOS__  |  tee  -a  ${gen_script_utf8}

popd
__EOS__

cat  <<-  __EOS__   \
    |  ${convert_to_sjis_command}       \
    |  tee  -a  ${gen_script_sjis}

POPD
PAUSE
__EOS__
